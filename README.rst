README
=========================

This is the README of a template for LaTeX beamer slides.

|
What is this repository for?
**********************************

* A slides template.

|  
How do I get set up?
********************************

* Copy this file and modify file name and content of the TEX file
  according to your needs. Compile with ``pdflatex``.
* Alternatively, a `makefile` is provided that can create 3 types of
  files with more or less the same content: some "empty" slides, that
  can be filled when teaching by using some "blackboard" software like
  `Xournal` or `Xournal++`; some "complete" slide, with the full
  content; some "print A4" slides, two slides per page, useful to be
  printed as hard copies. In this case, the usage is::

    ./makefile <YYY_AC_XX-topic>

  where the <> contains the name of the `tex` file with no extension.
* Dependencies: to compile this file you will need the ``unimi``
  beamer theme (`https://bitbucket.org/hydrogeo-unimi/unimi
  <https://bitbucket.org/hydrogeo-unimi/unimi>`_) and a folder
  containing some support `sty` files and images ``common``
  (`https://bitbucket.org/hydrogeo-unimi/common
  <https://bitbucket.org/hydrogeo-unimi/common>`_).

.. notes::
   - Some additional LaTeX packages will be probably be required, like
     for example ``siunitx`` (for example, on ubuntu, it is included
     in the ``texlive-science`` package). In general, you should be
     able to find out which package is missing by looking at the error
     log messages.

     If you don't need them, you can simply comment out the
     corresponding `usepackage` line.

Who do I talk to?
**********************************
* Main author: Alessandro Comunian
